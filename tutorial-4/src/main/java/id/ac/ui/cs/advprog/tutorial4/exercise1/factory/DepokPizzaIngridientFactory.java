package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.AlpineStyleCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.UiLakeClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.FasilkomStyleDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BbqSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngridientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new FasilkomStyleDough();
    }

    @Override
    public Sauce createSauce() {
        return new BbqSauce();
    }

    @Override
    public Cheese createCheese() {
        return new AlpineStyleCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BlackOlives(), new Garlic(), 
                             new Pineapple(), new RedPepper()};
        return veggies;
    }

    @Override
    public Clams createClam() {
        return new UiLakeClams();
    }
    
}
