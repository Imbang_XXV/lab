package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class AlpineStyleCheese implements Cheese {
    
    public String toString() {
        return "Special Alpine Style Cheese";
    }
}
