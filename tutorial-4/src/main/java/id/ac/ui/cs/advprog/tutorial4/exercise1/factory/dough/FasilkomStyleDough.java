package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FasilkomStyleDough implements Dough {
    
    public String toString() {
        return "Fasilkom Style Dough";
    }
}
