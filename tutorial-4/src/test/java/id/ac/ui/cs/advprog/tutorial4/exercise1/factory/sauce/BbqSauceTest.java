package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BbqSauceTest {
    private BbqSauce bbqSauce;
    
    @Before
    public void setUp() {
        bbqSauce = new BbqSauce();
    }
    
    @Test
    public void testMethodtoString() {
        assertEquals("Special BBQ Sauce", bbqSauce.toString());
    }
}
