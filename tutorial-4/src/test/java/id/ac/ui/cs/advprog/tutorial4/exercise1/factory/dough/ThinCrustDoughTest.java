package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {
    private ThinCrustDough thinCrustDough;
    
    @Before
    public void setUp() {
        thinCrustDough = new ThinCrustDough();
    }
    
    @Test
    public void testMethodtoString() {
        assertEquals("Thin Crust Dough", thinCrustDough.toString());
    }
}