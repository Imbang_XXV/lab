package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class DepokPizzaStoreTest {
    private DepokPizzaStore DepokPizzaStore;
    
    @Before
    public void setUp() {
        DepokPizzaStore = new DepokPizzaStore();
    }
    
    @Test
    public void runOrderPizzaMethod() {
        PizzaStore myStore = new DepokPizzaStore();

        Pizza pizza = myStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = myStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = myStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");
    }
}