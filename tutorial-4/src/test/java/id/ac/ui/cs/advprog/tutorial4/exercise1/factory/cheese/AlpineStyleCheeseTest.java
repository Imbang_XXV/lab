package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AlpineStyleCheeseTest {
    private AlpineStyleCheese alpineStyleCheese;
    
    @Before
    public void setUp() {
        alpineStyleCheese = new AlpineStyleCheese();
    }
    
    @Test
    public void testMethodtoString() {
        assertEquals("Special Alpine Style Cheese", alpineStyleCheese.toString());
    }
}
