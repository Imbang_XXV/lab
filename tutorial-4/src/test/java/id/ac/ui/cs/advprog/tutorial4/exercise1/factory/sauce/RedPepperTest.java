package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;

public class RedPepperTest {
    private RedPepper redPepper;
    
    @Before
    public void setUp() {
        redPepper = new RedPepper();
    }
    
    @Test
    public void testMethodtoString() {
        assertEquals("Red Pepper", redPepper.toString());
    }
}
