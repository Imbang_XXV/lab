package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;
    
    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }
    
    @Test
    public void runOrderPizzaMethod() {
        PizzaStore myStore = new NewYorkPizzaStore();

        Pizza pizza = myStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = myStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = myStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");
    }
}