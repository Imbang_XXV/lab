package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FasilkomStyleDoughTest {
    private FasilkomStyleDough fasilkomStyleDough;
    
    @Before
    public void setUp() {
        fasilkomStyleDough = new FasilkomStyleDough();
    }
    
    @Test
    public void testMethodtoString() {
        assertEquals("Fasilkom Style Dough", fasilkomStyleDough.toString());
    }
}