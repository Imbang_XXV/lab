package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple;

public class PineappleTest {
    private Pineapple pineapple;
    
    @Before
    public void setUp() {
        pineapple = new Pineapple();
    }
    
    @Test
    public void testMethodtoString() {
        assertEquals("Pineapple", pineapple.toString());
    }
}