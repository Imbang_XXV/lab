package id.ac.ui.cs.advprog.tutorial3.decorator.bread;


import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger extends Food{
    private double cost = 1.50;
        
    @Override
    public String getDescription() {
        return "Thin Bun Burger";
    }

    @Override
    public double cost() {
        return cost;
    }
    
}
