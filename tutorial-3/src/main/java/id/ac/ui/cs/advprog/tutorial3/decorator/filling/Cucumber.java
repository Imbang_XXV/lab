package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Filling{
    Food food;
    private double cost = 0.40;
    
    public Cucumber(Food food) {
        this.food = food;
    }
    @Override
    public String getDescription() {
        return food.getDescription() + ", adding cucumber";
    }

    @Override
    public double cost() {
        return food.cost() + cost;
    }

}
