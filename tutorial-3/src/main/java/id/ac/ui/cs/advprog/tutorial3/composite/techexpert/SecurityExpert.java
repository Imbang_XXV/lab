package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;

public class SecurityExpert extends Employees{
    
    public SecurityExpert(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        this.role = "Security Expert";
        if (salary < 70000) {
            throw new IllegalArgumentException();
        }
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
      
}
