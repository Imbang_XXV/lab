package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Filling {
    Food food;
    private double cost = 0.75;
    
    public Lettuce(Food food) {
        this.food = food;
    }
    
    @Override
    public String getDescription() {
        return food.getDescription() + ", adding lettuce";
    }

    @Override
    public double cost() {
        return food.cost() + cost;
    }

}
