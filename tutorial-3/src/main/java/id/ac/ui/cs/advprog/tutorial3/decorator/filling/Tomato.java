package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Filling {
    Food food;
    private double cost = 0.5;
    
    public Tomato(Food food) {
        this.food = food;
    }
    
    @Override
    public String getDescription() {
        return food.getDescription() + ", adding tomato";
    }

    @Override
    public double cost() {
        return food.cost() + cost;
    }

}
