package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    
    public static void main(String[] args) {
        //Thick Bun Burger with Beef Meat, Cheese, Cucumber, Lettuce, and Chili Sauce
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(thickBunBurgerSpecial.getDescription()+": "+thickBunBurgerSpecial.cost());

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()+": "+thickBunBurgerSpecial.cost());
        
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()+": "+thickBunBurgerSpecial.cost());
        
        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()+": "+thickBunBurgerSpecial.cost());
        
        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()+": "+thickBunBurgerSpecial.cost());
        
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        System.out.println(thickBunBurgerSpecial.getDescription()+": "+thickBunBurgerSpecial.cost());
    }
}
