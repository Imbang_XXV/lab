package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;

public class UiUxDesigner extends Employees{
    
    public UiUxDesigner(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        this.role = "UI/UX Designer";
        if (salary < 90000) {
            throw new IllegalArgumentException();
        }
        this.salary = salary;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
