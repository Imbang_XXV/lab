package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class NoCrustSandwich extends Food{
    private double cost = 2.00;
    
    @Override
    public String getDescription() {
        return "No Crust Sandwich";
    }

    @Override
    public double cost() {
        return cost;
    }




}
