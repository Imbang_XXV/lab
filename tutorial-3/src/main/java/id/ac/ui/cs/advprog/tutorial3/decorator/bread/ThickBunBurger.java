package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    private double cost = 2.50;
    
    @Override
    public String getDescription() {
        return "Thick Bun Burger";
    }

    @Override
    public double cost() {
        return cost;
    }

}
